FROM node:8

ARG PORT=3010

WORKDIR /home/app

COPY package.json yarn.lock ./

RUN yarn

COPY ./ ./

EXPOSE ${PORT}

CMD [ "yarn", "start" ]