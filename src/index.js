const express = require('express')
const mongoose = require('mongoose')
const routes = require('./routes')
require('dotenv').config()
const port = 3010

const app = express()

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
}, err => !err ? console.log('Mongo DB Connected.') : console.log(err))

// app.use(cors())
app.use(express.json())
app.use(routes)

app.listen(process.env.PORT || port, () => console.log(`Listening on port: ${port}`))
