const express = require('express')
const { index, show, store, update, destroy, test } = require('./controllers/ProdController')

const routes = express()

// Routes
routes.get('/prod', index)
routes.get('/prod/:id', show)
routes.post('/prod', store)
routes.put('/prod/:id', update)
routes.delete('/prod/:id', destroy)

// Health Check
routes.get('/test', test)

module.exports = routes;